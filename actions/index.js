module.exports = (app) => {
    console.log('Loading actions...');

    app.actions = {
        auth: require('./auth')(app),
        articles: require('./articles')(app),
        categories: require('./categories')(app)
    };
};
