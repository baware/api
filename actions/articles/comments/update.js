module.exports = (app) => {
    return function update(req, res, next) {
        req.comment.content = req.body.content ? req.body.content : req.comment.content;
        req.comment.pictureUrl = req.body.pictureUrl ? req.body.pictureUrl : req.comment.pictureUrl;

        req.comment.save().then((comment) => {
            res.send(comment);
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};