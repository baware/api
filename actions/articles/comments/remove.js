module.exports = (app) => {
    return function remove(req, res, next) {
        req.comment.destroy().then((comment) => {
            res.send(comment);
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};