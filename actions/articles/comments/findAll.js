module.exports = (app) => {
    return function findAll(req, res, next) {
        return res.send(req.article.comments);
    }
};