module.exports = (app) => {
    const Comment = app.models.Comment;

    return function create(req, res, next) {
        Comment.build(req.body).save().then((comment) => {
            let topic = "Article" + req.article.id;
            let payload = {
                notification: {
                    body: req.user.username + ": " + Buffer.from(comment.content, 'base64').toString(),
                    tag: req.article.id + "667",
                    title: Buffer.from(req.article.title, 'base64').toString(),
                    color: "#FF4081"
                },
                data: {
                    category: Buffer.from(req.article.title, 'base64').toString(),
                    type: "667",
                    pictureUrl: req.article.pictureUrl,
                    articleId: "" + req.article.id,
                    userId: "" + req.user.id
                }
            };

            let options = {
                collapseKey: req.article.id + "667"
            };

            app.firebase.messaging().sendToTopic(topic, payload, options).then((res) => {
                console.log("Successfully sent message:", res);
            }).catch((err) => {
                console.log("Error sending message:", err);
            });

            req.article.addComment(comment);
            req.user.addComment(comment);
            res.send(comment);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};