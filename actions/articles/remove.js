module.exports = (app) => {
    return function remove(req, res, next) {
        req.article.destroy().then((article) => {
            res.send(article);
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};
