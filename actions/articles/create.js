module.exports = (app) => {
    const Article = app.models.Article;

    return function create(req, res, next) {
        Article.build(req.body).save().then((article) => {
            let topic = "ArticlesFeed";
            let payload = {
                notification: {
                    body: Buffer.from(article.headlines, 'base64').toString(),
                    tag: "666",
                    title: Buffer.from(article.title, 'base64').toString(),
                    color: "#FF4081"
                },
                data: {
                    category: "Articles feed",
                    type: "666",
                    pictureUrl: article.pictureUrl,
                    articleId: "" + article.id,
                    userId: "" + req.user.id
                }
            };

            let options = {
                collapseKey: "666"
            };

            app.firebase.messaging().sendToTopic(topic, payload, options).then((res) => {
                console.log("Successfully sent message:", res);
            }).catch((err) => {
                console.log("Error sending message:", err);
            });

            req.user.addArticle(article);
            res.send(article);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
