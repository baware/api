module.exports = (app) => {
    return function update(req, res, next) {
        req.article.title = req.body.title ? req.body.title : req.article.title;
        req.article.headlines = req.body.headlines ? req.body.headlines : req.article.headlines;
        req.article.categoryId = req.body.categoryId ? req.body.categoryId : req.article.categoryId;
        req.article.content = req.body.content ? req.body.content : req.article.content;
        req.article.pictureUrl = req.body.pictureUrl ? req.body.pictureUrl : req.article.pictureUrl;

        req.article.save().then((article) => {
            res.send(article);
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};
