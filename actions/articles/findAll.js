module.exports = (app) => {
    const Article = app.models.Article;
    const User = app.models.User;
    const Category = app.models.Category;
    const Comment = app.models.Comment;

    return function findAll(req, res, next) {
        Article.findAll({
            order: [ ['createdAt', 'DESC'] ],
            include: [ { model: User }, { model: Category }, { model: Comment, as: 'comments', include: [ { model: User } ] } ]
        }).then((articles) => {
            if (!articles) return res.sendStatus(204);
            res.send(articles);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
