module.exports = (app) => {
    const Token = app.models.Token;

    return function logout(req, res, next) {
        Token.destroy({
            where: { userId: req.user.id }
        }).then((count) => {
            res.send({ count });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};