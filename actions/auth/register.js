const sha1 = require('sha1');

module.exports = (app) => {
    const User = app.models.User;

    return function register(req, res, next) {
        User.findOrCreate({
            where: { username: req.body.username },
            defaults: { password: sha1(req.body.password) }
        }).spread((user, created) => {
            if (created) return res.send(user);
            return res.status(401).send('Username already in use.');
        });
    }
};