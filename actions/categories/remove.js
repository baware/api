module.exports = (app) => {
    return function remove(req, res, next) {
        req.category.destroy().then((category) => {
            res.send(category);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};