module.exports = (app) => {
    const Category = app.models.Category;

    return function findAll(req, res, next) {
        Category.findAll({}).then((categories) => {
            if (!categories) return res.sendStatus(204);
            res.send(categories);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
