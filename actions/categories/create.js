module.exports = (app) => {
    const Category = app.models.Category;

    return function create(req, res, next) {
        Category.build(req.body).save().then((category) => {
            res.send(category);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};