module.exports = (app) => {
    return function update(req, res, next) {
        req.category.name = req.body.name ? req.body.name : req.category.name;

        req.category.save().then((data) => {
            res.send(data);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};