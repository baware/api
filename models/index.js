module.exports = (app) => {
    console.log('Loading models...');

    app.models = {
        Article: require('./mysql/article')(app),
        Category: require('./mysql/category')(app),
        Comment: require('./mysql/comment')(app),
        Token: require('./mysql/token')(app),
        User: require('./mysql/user')(app)
    };

    app.models.Article.belongsTo(app.models.User);
    app.models.Article.belongsTo(app.models.Category);
    app.models.Article.hasMany(app.models.Comment, { onDelete: 'CASCADE' });

    app.models.Comment.belongsTo(app.models.Article);
    app.models.Comment.belongsTo(app.models.User);

    app.models.Token.belongsTo(app.models.User);

    app.models.User.hasMany(app.models.Article, { onDelete: 'CASCADE' });
    app.models.User.hasMany(app.models.Comment, { onDelete: 'CASCADE' });
    app.models.User.hasMany(app.models.Token, { onDelete: 'CASCADE' });

};
