const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('comment', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        content: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        pictureUrl: {
            type: Sequelize.STRING,
            allowNull: true
        }
    });
};
