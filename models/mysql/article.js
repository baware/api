const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('article', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        headlines: {
            type: Sequelize.STRING,
            allowNull: false
        },
        content: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        pictureUrl: {
            type: Sequelize.STRING,
            allowNull: true
        }
    });
};
