let admin = require('firebase-admin');

module.exports = (app, resolve, reject) => {
    app.firebase = admin;
    app.firebase.initializeApp({
        credential: admin.credential.cert(require(app.settings.firebase.keyPath)),
        databaseURL: app.settings.firebase.databaseURL
    });
};
