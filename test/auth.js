let expect = require('chai').expect;
let request = require('request');

describe('API Authentification', function() {
    describe('[POST] /auth/login', function() {
        let url = 'http://localhost:3002/auth/login';
        let data = {
            username: 'Kevin',
            password: 'pouet'
        };

        it('returns status 200', function() {
            request(url, data, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns id: 1', function() {
            request(url, data, function(error, response, body) {
                expect(body.id).to.equal(1);
            });
        });

        it('returns username: Kevin', function() {
            request(url, data, function(error, response, body) {
                expect(body.username).to.equal(Kevin);
            });
        });
    });

    describe('[GET] /auth/logout', function() {
        let url = 'http://localhost:3002/auth/logout';

        it('returns status 200', function() {
            request(url, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns count >= 0', function() {
            request(url, function(error, response, body) {
                expect(body.count).to.least(0);
            });
        });
    });

    describe('[POST] /auth/register', function() {
        let url = 'http://localhost:3002/auth/register';
        let data = {
            username: 'Kevin',
            password: 'pouet'
        };

        it('returns status 401', function() {
            request(url, data, function(error, response, body) {
                expect(response.statusCode).to.equal(401);
            });
        });

        it('returns "Username already in use"', function() {
            request(url, data, function(error, response, body) {
                expect(body).to.equal("Username already in use");
            });
        });
    });
});