let expect = require('chai').expect;
let request = require('request');

describe('ARTICLE CRUD', function(){

    describe('[GET] /articles', function() {
        let url = 'http://localhost:3002/articles';
        it('should return status 200', function() {
            request(url, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });
    });

    describe('[POST] /articles', function() {
        let url = 'http://localhost:3002/articles';
        let data = {
            pictureUrl : "http://www.20min.ch/diashow/208257/DAF23900E9CA34C24AC7A98E6D975280.jpg",
            userId : 2,
            categoryId : 4,
            title : new Buffer("La Cyberattaque a été rendue possible par la NSA").toString("base64"),
            content: new Buffer("De la Russie à l'Espagne et du Mexique à l'Australie, des dizaines de milliers d'ordinateurs ont été infectés vendredi par un logiciel de rançon exploitant une faille dans les systèmes Windows, divulguée dans des documents piratés de l'agence de sécurité américaine NSA. Le service public de santé britannique (NHS), cinquième employeur du monde avec 1,7 million de salariés, semble avoir été la principale victime -- et potentiellement la plus inquiétante en mettant en danger des patients -- de ces attaques.Mais il est loin d'être le seul. Le constructeur automobile français Renault a indiqué samedi à l'AFP avoir été affecté et des sites de production étaient à l'arrêt en France mais aussi en Slovénie, dans la filiale de Renault, Revoz. La Banque centrale russe a annoncé samedi que le système bancaire du pays avait été visé par la cyberattaque, ainsi que plusieurs ministères, et que les pirates avaient tenté de forcer les installations informatiques du réseau ferroviaire").toString('base64'),
            headlines : new Buffer("Une vague de cyberattaques «sans précédent», selon Europol, a frappé samedi une centaine de pays, affectant le fonctionnement de nombreuses entreprises et organisations.").toString('base64'),
        };
        it('should return status 200', function() {
            request(url, data, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns pictureUrl:http://www.20min.ch/diashow/208257/DAF23900E9CA34C24AC7A98E6D975280.jpg', function(){
            request(url, data, function(error, response, body) {
                expect(body.pictureUrl).to.equal("http://www.20min.ch/diashow/208257/DAF23900E9CA34C24AC7A98E6D975280.jpg");
            });
        });

        it('returns id:4', function(){
            request(url, data, function(error, response, body) {
                expect(body.id).to.equal(4);
            });
        })
    });

    describe('[PUT] /articles/:articleId', function(){
        let url = 'http://localhost:3002/articles/4';
        let data = {
            pictureUrl : "http://cdn3-europe1.new2.ladmedia.fr/var/europe1/storage/images/europe1/technologies/ce-que-lon-sait-de-la-cyberattaque-en-cours-dans-le-monde-entier-depuis-vendredi-3329048/41130146-1-fre-FR/Ce-que-l-on-sait-de-la-cyberattaque-en-cours-dans-le-monde-entier-depuis-vendredi.jpg",
            userId : 2,
            categoryId : 4,
            title : new Buffer("La Cyberattaque a été rendue possible par la NSA").toString("base64"),
            content: new Buffer("De la Russie à l'Espagne et du Mexique à l'Australie, des dizaines de milliers d'ordinateurs ont été infectés vendredi par un logiciel de rançon exploitant une faille dans les systèmes Windows, divulguée dans des documents piratés de l'agence de sécurité américaine NSA. Le service public de santé britannique (NHS), cinquième employeur du monde avec 1,7 million de salariés, semble avoir été la principale victime -- et potentiellement la plus inquiétante en mettant en danger des patients -- de ces attaques.Mais il est loin d'être le seul. Le constructeur automobile français Renault a indiqué samedi à l'AFP avoir été affecté et des sites de production étaient à l'arrêt en France mais aussi en Slovénie, dans la filiale de Renault, Revoz. La Banque centrale russe a annoncé samedi que le système bancaire du pays avait été visé par la cyberattaque, ainsi que plusieurs ministères, et que les pirates avaient tenté de forcer les installations informatiques du réseau ferroviaire").toString('base64'),
            headlines : new Buffer("Une vague de cyberattaques «sans précédent», selon Europol, a frappé samedi une centaine de pays, affectant le fonctionnement de nombreuses entreprises et organisations.").toString('base64'),
        };

        it('should return status 200', function() {
            request(url, data, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns pictureUrl:http://cdn3-europe1.new2.ladmedia.fr/var/europe1/storage/images/europe1/technologies/ce-que-lon-sait-de-la-cyberattaque-en-cours-dans-le-monde-entier-depuis-vendredi-3329048/41130146-1-fre-FR/Ce-que-l-on-sait-de-la-cyberattaque-en-cours-dans-le-monde-entier-depuis-vendredi.jpg', function(){
            request(url, data, function(error, response, body) {
                expect(body.pictureUrl).to.equal("http://cdn3-europe1.new2.ladmedia.fr/var/europe1/storage/images/europe1/technologies/ce-que-lon-sait-de-la-cyberattaque-en-cours-dans-le-monde-entier-depuis-vendredi-3329048/41130146-1-fre-FR/Ce-que-l-on-sait-de-la-cyberattaque-en-cours-dans-le-monde-entier-depuis-vendredi.jpg");
            });
        });
    });

    describe('[GET] /articles/:articleId', function(){
        let url = 'http://localhost:3002/articles/4';
        let data = {};

        it('should return status 200', function() {
            request(url, data, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns userId:2', function(){
            request(url, data, function(error, response, body) {
                expect(body.userId).to.equal(2);
            });
        });
    });

    describe('[DELETE] /articles/:articleId', function(){
        let url = 'http://localhost:3002/articles/4';
        let data = {};

        it('should return status 200', function() {
            request(url, data, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns id:null', function(){
            request(url, data, function(error, response, body) {
                expect(body.userId).to.equal(null);
            });
        });
    });

    describe('[GET] /articles/:articleId/comments', function(){
        let url = 'http://localhost:3002/articles/3';

        it('should return status 200', function() {
            request.get(url, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns comments.length: 2', function(){
            request.get(url, function(error, response, body) {
                expect(body.length).to.equal(2);
            });
        });
    });

    describe('[DELETE] /articles/:articleId/comments/:commentId', function(){
        let url = 'http://localhost:3002/articles/2/comments/1';

        it('should return status 200', function() {
            request.post(url, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns count >= 0', function(){
            request(url, function(error, response, body) {
                expect(body.count).to.least(0);
            });
        });
    });

    describe('[POST] /articles/2/comments', function() {
        let url = 'http://localhost:3002/articles/2/comments';
        let data = {
            userId: 2,
            articleId: 2,
            content: new Buffer("Il faut aussi se demander ce que fait la Turquie en ligue Europa! Je plains le supporter Lyonnais qui va avoir l'audace ou l'inconscience d'aller à Istanbul soutenir son Club.").toString('base64')
        };

        it('returns userId: 2', function(){
            request.post(url, data, function(error, response, body) {
                expect(body.userId).to.equal(2);
            });
        });

        it('returns articleId: 2', function(){
            request(url, data, function(error, response, body) {
                expect(body.articleId).to.equal(2);
            });
        })
    });

});
