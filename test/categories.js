let expect = require('chai').expect;
let request = require('request');

describe('API Categories', function() {
    describe('[GET] /categories/', function() {
        let url = 'http://localhost:3002/categories';

        it('return status 200', function() {
            request(url, function(error, response) {
                expect(response.statusCode).to.equal(200);
            });
        });
    });

    describe('[GET] /categories/2', function() {
        let url = 'http://localhost:3002/categories/2';

        it('returns status 200', function() {
            request(url, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns {"id": 2, "name": "Health", "createdAt": "2017-05-13T14:23:04.000Z", "updatedAt": "2017-05-13T14:23:04.000Z"}', function() {
            request(url, function(error, response, body) {
                expect(body).to.equal({"id": 2, "name": "Health", "createdAt": "2017-05-13T14:23:04.000Z", "updatedAt": "2017-05-13T14:23:04.000Z"});
            });
        });
    });

    describe('[PUT] /categories/5', function() {
        let url = 'http://localhost:3002/categories/5';
        let data = {
            "name": "Test123"
        };

        it('returns id: 5', function() {

            request.put(url, data, function(error, response, body) {
                expect(body.id).to.equal(5);
            });
        });

        it('returns name: "Test123"', function() {
            request.put(url, function(error, response, body) {
                expect(body.name).to.equal("Test123");
            });
        });
    });
  
    describe('[POST] /categories/', function() {
        let url = 'http://localhost:3002/categories/';
        let data = {
            "name": "Test"
        };

        it('returns status 200', function() {
            request.post(url, data, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('returns name: Test', function() {
            request.post(url, data, function(error, response, body) {
                expect(body.name).to.equal("Test");
            });
        });
    });

});