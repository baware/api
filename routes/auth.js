const router = require('express').Router();

module.exports = (app) => {

    router.post('/login',
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.authentifications,
        app.actions.auth.login
    );

    router.get('/logout',
        app.middlewares.security.isAuthenticated,
        app.actions.auth.logout
    );

    router.post('/register',
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.users,
        app.actions.auth.register
    );
    
    return router;
};
