module.exports = (app) => {
    console.log('Loading routes...\n');

    app.use('/auth', require('./auth')(app));
    app.use('/articles', require('./articles')(app));
    app.use('/categories', require('./categories')(app));
};
