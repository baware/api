const router = require('express').Router();

module.exports = (app) => {
    router.get('/',
        app.actions.categories.findAll
    );

    router.get('/:categoryId',
        app.middlewares.parsers.categories,
        app.actions.categories.findOne
    );

    router.post('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAdmin,
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.categories,
        app.actions.categories.create
    );

    router.put('/:categoryId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAdmin,
        app.middlewares.parsers.categories,
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.categories,
        app.actions.categories.update
    );

    router.delete('/:categoryId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.security.isAdmin,
        app.middlewares.parsers.categories,
        app.actions.categories.remove
    );
    
    return router;
};
