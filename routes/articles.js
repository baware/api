const router = require('express').Router();

module.exports = (app) => {
    router.get('/',
        app.actions.articles.findAll
    );

    router.get('/:articleId',
        app.middlewares.parsers.articles,
        app.actions.articles.findOne
    );

    router.post('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.articles,
        app.actions.articles.create
    );

    router.put('/:articleId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.articles,
        app.middlewares.security.isOwner,
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.articles,
        app.actions.articles.update
    );

    router.delete('/:articleId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.articles,
        app.actions.articles.remove
    );

    router.get('/:articleId/comments',
        app.middlewares.parsers.articles,
        app.actions.articles.comments.findAll
    );

    router.post('/:articleId/comments',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.articles,
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.comments,
        app.actions.articles.comments.create
    );

    router.put('/:articleId/comments/:commentId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.articles,
        app.middlewares.parsers.comments,
        app.middlewares.bodyparser.json(),
        app.middlewares.validators.comments,
        app.actions.articles.comments.update
    );

    router.delete('/:articleId/comments/:commentId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.articles,
        app.middlewares.parsers.comments,
        app.actions.articles.comments.remove
    );
    
    return router;
};
