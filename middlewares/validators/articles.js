module.exports = (req , res, next) => {
    if (!req.body || !req.body.title || !req.body.headlines || !req.body.content || !req.body.categoryId) {
        return res.status(400).send('Missing article fields (title, headlines, content, categoryId, pictureUrl?)');
    }

    if (req.body.title.length < 5) {
        return res.status(400).send('Wrong title value. (min length: 5)');
    }

    if (req.body.headlines.length < 5) {
        return res.status(400).send('Wrong headlines value. (min length: 5)');
    }

    if (req.body.content.length < 5) {
        return res.status(400).send('Wrong content value. (min length: 5)');
    }

    let categoryId = Number(req.body.categoryId);
    if (isNaN(categoryId)) {
        return res.status(400).send('Wrong category value. (integer)');
    }

    if (req.body.pictureUrl) {
        let urlRegExp = new RegExp(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/);
        if (!urlRegExp.test(req.body.pictureUrl)) {
            return res.status(400).send('Wrong pictureUrl value. (must be URL)');
        }
    }

    return next();
};
