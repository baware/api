module.exports = (app) => {
    return {
        articles: require('./articles'),
        authentifications: require('./authentifications'),
        categories: require('./categories'),
        comments: require('./comments'),
        users: require('./users')
    }
};