module.exports = (req , res, next) => {
    if (!req.body || !req.body.content) {
        return res.status(400).send('Missing article fields (content, pictureUrl?)');
    }

    if (req.body.content.length < 1) {
        return res.status(400).send('Wrong content value. (min length: 1)');
    }

    if (req.body.pictureUrl) {
        let urlRegExp = new RegExp(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/);
        if (!urlRegExp.test(req.body.pictureUrl)) {
            return res.status(400).send('Wrong pictureUrl value. (must be URL)');
        }
    }

    return next();
};
