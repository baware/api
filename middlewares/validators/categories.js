module.exports = (req , res, next) => {
    if (!req.body || !req.body.name) {
        return res.status(400).send('Missing category fields (name)');
    }

    if (req.body.name.length < 2) {
        return res.status(400).send('Wrong name value. (min length: 2)');
    }

    return next();
};