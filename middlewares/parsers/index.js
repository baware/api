module.exports = (app) => {
    return {
        articles: require('./articles')(app),
        categories: require('./categories')(app),
        comments: require('./comments')(app)
    }
};