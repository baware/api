module.exports = (app) => {
    const User = app.models.User;
    const Comment = app.models.Comment;

    return (req, res, next) => {
        Comment.findOne({
            where: { id: req.params.commentId },
            include: [ { model: User } ]
        }).then((data) => {
            if (!data) return res.status(404).send('Comment not found');
            req.comment = data;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
