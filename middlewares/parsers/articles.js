module.exports = (app) => {
    const Article = app.models.Article;
    const User = app.models.User;
    const Category = app.models.Category;
    const Comment = app.models.Comment;

    return (req, res, next) => {
        Article.findOne({
            where: { id: req.params.articleId },
            include: [ { model: User }, { model: Category }, { model: Comment, as: 'comments', include: [ { model: User } ] } ]
        }).then((data) => {
            if (!data) return res.status(404).send('Article not found');
            req.article = data;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
