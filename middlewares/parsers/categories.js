module.exports = (app) => {
    const Category = app.models.Category;

    return (req, res, next) => {
        Category.findOne({
            where: { id: req.params.categoryId }
        }).then((data) => {
            if (!data) return res.status(404).send('Category not found');
            req.category = data;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};