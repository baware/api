module.exports = (app) => {
    return {
        isAdmin: require('./isAdmin')(app),
        isAuthenticated: require('./isAuthenticated')(app),
        isOwner: require('./isOwner')(app)
    }
};