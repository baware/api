module.exports = (app) => {
    return (req, res, next) => {
        if(!req.user.isAdmin) return res.status(403).send('Administrator privileges required');

        next();
    }
};