module.exports = (app) => {
    return (req, res, next) => {
        if(req.user.id !== req.article.userId) return res.status(403).send('Not your article');

        next();
    }
};
