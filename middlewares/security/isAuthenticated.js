const jwtk = require('jsonwebtoken');

module.exports = (app) => {
    const Token = app.models.Token;
    const User = app.models.User;
    const Article = app.models.Article;

    return (req, res, next) => {
        if(!req.headers || !req.headers.authorization) return res.status(403).send('Authentication required');

        jwtk.verify(req.headers.authorization, app.settings.security.salt, null, (err, decryptedToken) => {
            if (err) return res.status(401).send('Invalid token');

            Token.findOne({
                where: { id: decryptedToken.tokenId }
            }).then((token) => {
                if(!token) return res.status(403).send('Invalid token');

                User.findOne({
                    where: { id: token.userId },
                    include: [ { model: Article } ]
                }).then((data) => {
                    if (!data) return res.status(403).send('No user attached to this token');
                    req.user = data;
                    next();
                }).catch((err) => {
                    res.status(500).send(err.message);
                });
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        });
    }
};
