module.exports = (app) => {
    console.log('Loading middlewares...');

    app.middlewares = {
        bodyparser: require('body-parser'),
        parsers: require('./parsers')(app),
        security: require('./security')(app),
        validators: require('./validators')(app)
    };
};
